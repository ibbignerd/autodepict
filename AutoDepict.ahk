; Created by: ibbignerd
;
; Description: A quick link/lookup for Cydia depictions
;
; TOS: Use at your own risk. This is obviously open source so look through the code.
;
; License: Modify, Re-distribute, No Commercial, CC License
;

#SingleInstance, force

Load:
RedditHotkeyInput =
ViewHotkeyInput =
Edit1 = http://apt.thebigboss.org/repofiles/cydia/dists/stable/main/binary-iphoneos-arm/Packages
Edit2 = http://apt.modmyi.com/dists/stable/main/binary-iphoneos-arm/Packages
Edit3 =
Edit4 =
Edit5 =
Edit6 =
Edit7 =
IfExist, %A_ScriptDir%\AutoDepict\variables.txt
{
    Loop, Read, %A_ScriptDir%\AutoDepict\variables.txt
    {
        StringSplit, line, A_LoopReadLine, |
        if line1 = RedditHotkeyInput
            RedditHotkeyInput := line2
        else if line1 = ViewHotkeyInput
            ViewHotkeyInput := line2
        else if line1 = Edit1
            Edit1 := line2
        else if line1 = Edit2
            Edit2 := line2
        else if line1 = Edit3
            Edit3 := line2
        else if line1 = Edit4
            Edit4 := line2
        else if line1 = Edit5
            Edit5 := line2
        else if line1 = Edit6
            Edit6 := line2
        else if line1 = Edit7
            Edit7 := line2
    }
}
goto, showGUI
return

showGUI:

    Gui, Add, Text, x12 y23 w50 h20 , BigBoss
    Gui, Add, Edit, x72 y20 w390 h20 vEdit1, %Edit1%

    Gui, Add, Text, x12 y53 w50 h20 , ModMyi
    Gui, Add, Edit, x72 y50 w390 h20 vEdit2, %Edit2%

    Gui, Add, Text, x12 y83 w50 h20 , 3rd party
    Gui, Add, Edit, x72 y80 w390 h20 vEdit3, %Edit3%

    Gui, Add, Text, x12 y113 w50 h20 , 3rd party
    Gui, Add, Edit, x72 y110 w390 h20 vEdit4, %Edit4%

    Gui, Add, Text, x12 y143 w50 h20 , 3rd party
    Gui, Add, Edit, x72 y140 w390 h20 vEdit5, %Edit5%

    Gui, Add, Text, x12 y173 w50 h20 , 3rd party
    Gui, Add, Edit, x72 y170 w390 h20 vEdit6, %Edit6%

    Gui, Add, Text, x12 y203 w50 h20 , 3rd party
    Gui, Add, Edit, x72 y200 w390 h20 vEdit7, %Edit7%

    Gui, Add, Text, x12 y243 w60 h20 , Reddit Link
    Gui, Add, Hotkey, x72 y240 w70 h20 vRedditHotkeyInput, %RedditHotkeyInput%
    Gui, Add, Text, x152 y243 w50 h20 , View Link
    Gui, Add, Hotkey, x205 y240 w70 h20 vViewHotkeyInput, %ViewHotkeyInput%
    Gui, Add, Button, x297 y240 w60 h30 , Save
    Gui, Add, Button, x372 y240 w90 h30 , Parse Files

    Gui, Show, x127 y87 h317 w479, AutoDepict By /u/ibbignerd
    if (RedditHotkeyInput != "") && (ViewHotkeyInput != "")
    {
        gosub, ButtonSave
    }
    goto, popArray
Return

GuiClose:
    Gui, Destroy
    goto, Save
return

Save:
    FileDelete, %A_ScriptDir%\AutoDepict\variables.txt
    GuiControlGet, RedditHotkeyInput
    GuiControlGet, ViewHotkeyInput
    GuiControlGet, Edit1
    GuiControlGet, Edit2
    GuiControlGet, Edit3
    GuiControlGet, Edit4
    GuiControlGet, Edit5
    GuiControlGet, Edit6
    GuiControlGet, Edit7
    addToFile = RedditHotkeyInput|%RedditHotkeyInput%`nViewHotkeyInput|%ViewHotkeyInput%`nEdit1|%Edit1%`nEdit2|%Edit2%`nEdit3|%Edit3%`nEdit4|%Edit4%`nEdit5|%Edit5%`nEdit6|%Edit6%`nEdit7|%Edit7%`n
    FileAppend, %addToFile%, %A_ScriptDir%\AutoDepict\variables.txt
return

ButtonSave:
    GuiControlGet, RedditHotkeyInput
    GuiControlGet, ViewHotkeyInput

    if RedditHotkeyUsed !=
    {
        Hotkey, %RedditHotkeyUsed%,,Off
        RedditHotkeyUsed =
    }
    RedditHotkeyUsed = %RedditHotkeyInput%
    Hotkey, %RedditHotkeyUsed%, RedditHotkey

    if ViewHotkeyUsed !=
    {
        Hotkey, %ViewHotkeyUsed%,,Off
        ViewHotkeyUsed =
    }
    ViewHotkeyUsed = %ViewHotkeyInput%
    Hotkey, %ViewHotkeyUsed%, ViewHotkey

    Gui, Add, Text, x2 y280 w470 h30 , Hotkey Saved!
    sleep, 200
    Gosub, Save
    goto, popArray
return

ButtonParseFiles:
    IfNotExist, %A_ScriptDir%\AutoDepict\Packages\
      FileCreateDir, %A_ScriptDir%\AutoDepict\Packages\
    IfNotExist, %A_ScriptDir%\AutoDepict\Cache\
      FileCreateDir, %A_ScriptDir%\AutoDepict\Cache\
    GuiControlGet, Edit1
    GuiControlGet, Edit2
    GuiControlGet, Edit3
    GuiControlGet, Edit4
    GuiControlGet, Edit5
    GuiControlGet, Edit6
    GuiControlGet, Edit7
    if Edit1 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit1%
        UrlDownloadToFile, %Edit1%, %A_ScriptDir%\AutoDepict\Packages\Edit1.txt
    }
    if Edit2 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit2%
        UrlDownloadToFile, %Edit2%, %A_ScriptDir%\AutoDepict\Packages\Edit2.txt
    }
    if Edit3 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit3%
        UrlDownloadToFile, %Edit3%, %A_ScriptDir%\AutoDepict\Packages\Edit3.txt
    }
    if Edit4 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit4%
        UrlDownloadToFile, %Edit4%, %A_ScriptDir%\AutoDepict\Packages\Edit4.txt
    }
    if Edit5 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit5%
        UrlDownloadToFile, %Edit5%, %A_ScriptDir%\AutoDepict\Packages\Edit5.txt
    }
    if Edit6 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit6%
        UrlDownloadToFile, %Edit6%, %A_ScriptDir%\AutoDepict\Packages\Edit6.txt
    }
    if Edit7 !=
    {
        Gui, Add, Text, x2 y280 w470 h30 , Downloading: %Edit7%
        UrlDownloadToFile, %Edit7%, %A_ScriptDir%\AutoDepict\Packages\Edit7.txt
    }
    Gui, Add, Text, x2 y280 w470 h30 , Downloading: Completed!

    Loop, %A_ScriptDir%\AutoDepict\Packages\*.txt,0
    {
        filename = %A_LoopFileName%
        Gui, Add, Text, x2 y280 w470 h30 , Parsing: %filename%
        FileDelete, %A_ScriptDir%\AutoDepict\Cache\%filename%
        addToFile =
        Loop, Read, %A_LoopFileFullPath%
        {
            StringReplace, LoopReadLine, A_LoopReadLine, `n, newpackage
            type := SubStr(LoopReadLine, 1, 5)
            dev := SubStr(LoopReadLine, 1, 4)
            if type = Name:
            {
                StringReplace, packageName, LoopReadLine, Name:%A_Space%,, ALL
            }else
            if type = Depic
            {
                StringReplace, packageURL, LoopReadLine, Depiction:%A_Space%,,ALL
                addToFile = %addToFile%%packageName%, %packageURL%`n
            }
        }
        FileAppend, %addToFile%, %A_ScriptDir%\AutoDepict\Cache\%filename%

    }
    Gui, Add, Text, x2 y280 w470 h30 , Parsing: Completed!
    goto, popArray
return


popArray:
    Gui, Add, Text, x2 y280 w470 h30 , Storing Variables
    dataArray := {}
    Loop,  %A_ScriptDir%\AutoDepict\Cache\*.txt,0
    {
        Loop, Read, %A_LoopFileFullPath%
        {
            masterIndex = %A_Index%
            Loop, Parse, A_LoopReadLine, CSV
            {
                index = %A_Index%
                ifEqual, index, 1
                {
                    name = %A_LoopField%
                }
                if A_Index = 2
                {
                    url = %A_LoopField%
                    dataArray.Insert(name, url)
                }
            }
        }
    }
    Gui, Add, Text, x2 y280 w470 h30 ,
return

ViewHotkey:
    sleep, 200
    send, ^c
    input = %clipboard%
    results := {}
    key =
    value =

    For key, value in dataArray
    {
        needle := "i)^"input
        if RegExMatch(key, needle, i) > 0
        {
            results.Insert(key, value)
        }
    }
    maxArray = 0
    for key, value in results
        maxArray += 1
    if maxArray = 0
    {
        msgbox, "%input%" Not Found!
        return
    }
    Else if maxArray = 1
    {
        finalValue := results[input]
        run %finalValue%
        return
    }Else
    {
        for key, value in results
        {
            Menu , AutoDepict , Add , %key% , ViewOptionSelect
            Menu , AutoDepict , Icon , %key% ,
        }
        Menu , AutoDepict , Show
    }
    results := ""
    menu, AutoDepict, DeleteAll
return

ViewOptionSelect:
    value := results[A_ThisMenuItem]
    run %value%
    menu, AutoDepict, DeleteAll
return

RedditHotkey:
    sleep, 200
    send, ^c
    input = %clipboard%
    results := {}
    key =
    value =

    For key, value in dataArray
    {
        needle := "i)^"input
        if RegExMatch(key, needle, i) > 0
        {
            results.Insert(key, value)
        }
    }
    maxArray = 0
    for key, value in results
        maxArray += 1
    if maxArray = 0
    {
        msgbox, "%input%" Not Found!
        return
    }
    Else if maxArray = 1
    {
        finalValue := results[input]
        clipboard = [%input%](%finalValue%)
        send, ^v
        return
    }Else
    {
        for key, value in results
        {
            Menu , AutoDepict , Add , %key% , RedditOptionSelect
            Menu , AutoDepict , Icon , %key% ,
        }
        Menu , AutoDepict , Show
    }
    results := ""
    menu, AutoDepict, DeleteAll
return


RedditOptionSelect:
    key = %A_ThisMenuItem%
    value := results[A_ThisMenuItem]
    clipboard = [%key%](%value%)
    send, ^v
    menu, AutoDepict, DeleteAll
return

; SmoothKB
; FlagPaint7 Lite
; Data Logo Switcher
; YourTube HD
; CallBar (iOS 7 & 8)
; Flat 7 Redesign Theme
; derpaderpaderp
